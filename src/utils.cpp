/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 Marcus Britanicus
 * Implementation of the desq-shell-unstable-v1 protocol
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "utils.hpp"
// #include "wayfire-view-lists.hpp"

wf::output_t * get_output_from_id( uint output_id ) {
    if ( output_id ) {
        for ( wf::output_t *wf_output : wf::get_core().output_layout->get_outputs() ) {
            if ( wf_output->get_id() == output_id ) {
                return wf_output;
            }
        }
    }

    return wf::get_core().seat->get_active_output();
}


bool check_view_toplevel( wayfire_view view ) {
    if ( view == nullptr ) {
        return false;
    }

    if ( view->is_mapped() == false ) {
        return false;
    }

    if ( view->role != wf::VIEW_ROLE_TOPLEVEL ) {
        return false;
    }

    if ( view->get_output() == nullptr ) {
        return false;
    }

    return true;
}


wayfire_toplevel_view get_view_from_id( uint view_id ) {
    std::vector<wayfire_view> view_vector;
    wayfire_view              view;

    view_vector = wf::get_core().get_all_views();

    // there is no view_id 0 use it to get the active view
    if ( view_id == 0 ) {
        view = wf::get_core().get_cursor_focus_view();

        if ( check_view_toplevel( view ) ) {
            return wf::toplevel_cast( view );
        }
    }

    for (auto it = view_vector.begin(); it != view_vector.end(); ++it) {
        view = *it;

        if ( check_view_toplevel( view ) ) {
            if ( view->get_id() == view_id ) {
                return wf::toplevel_cast( view );
            }
        }
    }

    return nullptr;
}


uint32_t get_view_state( wayfire_toplevel_view view ) {
    /** Retrieve states */
    uint32_t state = 0;

    // // active
    // if ( view->activated ) {
    //     state |= WindowState::Active;
    // }
    //
    // // minimized
    // if ( view->minimized ) {
    //     state |= WindowState::Minimized;
    // }
    //
    // // maximized
    // if ( view->toplevel()->current().tiled_edges == wf::TILED_EDGES_ALL ) {
    //     state |= WindowState::Maximized;
    // }
    //
    // // fullscreen
    // if ( view->toplevel()->current().fullscreen ) {
    //     state |= WindowState::Fullscreen;
    // }
    //
    // // pin_above
    // if ( view->has_data( "wm-actions-above" ) ) {
    //     state |= WindowState::PinnedAbove;
    // }
    //
    // // pin_below
    // if ( view->has_data( "wm-actions-below" ) ) {
    //     state |= WindowState::PinnedBelow;
    // }
    //
    // // sticky
    // if ( view->sticky ) {
    //     state |= WindowState::Sticky;
    // }
    //
    // // demands_attention
    // if ( view->has_data( "demands-attention" ) ) {
    //     state |= WindowState::DemandsAttention;
    // }

    return state;
}
