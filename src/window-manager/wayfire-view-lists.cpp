/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 Marcus Britanicus
 * Implementation of the desq-shell-unstable-v1 protocol
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <iostream>

#include <wayfire/output.hpp>
#include <wayfire/workspace-set.hpp>

#include "utils.hpp"
#include "wayfire-view-lists.hpp"
#include "wayfire-view-lists-server.h"
#include "wayfire-view-lists-server.hpp"

Wayfire::WM::ViewLister::ViewLister( struct ::wl_display *display, int version ) : Wayland::Server::WayfireViewLister( display, version ) {
    watchForSignals();
}


Wayfire::WM::ViewLister::~ViewLister() {
}


void Wayfire::WM::ViewLister::locateView( Resource *, uint32_t uuid ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        /** Get our output */
        wf::output_t *op  = view->get_output();
        uint32_t     opId = (op == nullptr ? 0 : op->get_id() );

        /** If the output is not null, get the workspace-set */
        auto     wset   = (op == nullptr ? nullptr : op->wset() );
        uint32_t wsetId = (wset == nullptr ? 0 : wset->get_index() );

        /** If the wset is not null, get the workspace */
        auto workspace = (wset == nullptr ? wf::point_t{ -1, -1 } : wset->get_view_main_workspace( view ) );

        sendViewLocated( uuid, opId, wsetId, workspace.y, workspace.x );
    }
}


void Wayfire::WM::ViewLister::watchForSignals() {
    on_view_mapped =
        [ = ] ( auto signal ) {
            std::cerr << "-------------------> " << signal->view->get_id() << "   " << ( signal->view->role == wf::VIEW_ROLE_TOPLEVEL ) << std::endl;
            if ( signal->view->role == wf::VIEW_ROLE_TOPLEVEL ) {
                Wayland::Server::WayfireViewHandle *view = new Wayland::Server::WayfireViewHandle();
                std::cerr << "Sending view-added (" << signal->view->get_id() << ") to " << m_resource_map.size() << " clients" << std::endl;
                for( auto crPair: m_resource_map ) {
                    auto res = view->add( crPair.first, 1 );
                    sendViewMapped( signal->view->get_id(), res->handle );
                }
            }
        };

    wf::get_core().connect( &on_view_mapped );
}
