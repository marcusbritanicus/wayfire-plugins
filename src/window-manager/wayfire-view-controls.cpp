/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 Marcus Britanicus
 * Implementation of the desq-shell-unstable-v1 protocol
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <signal.h>

#include <wayfire/core.hpp>
#include <wayfire/seat.hpp>
#include <wayfire/view.hpp>
#include <wayfire/output.hpp>
#include <wayfire/toplevel.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/toplevel-view.hpp>
#include <wayfire/window-manager.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/plugins/wm-actions-signals.hpp>

#include "wayfire-view-controls-server.hpp"
#include "wayfire-view-controls.hpp"
#include "utils.hpp"

#include <iostream>
#include <utility>

Wayfire::WM::ViewController::ViewController( struct ::wl_display *display, int version ): Wayland::Server::WayfireViewController( display, version ) {
    // Nothing to do from our side here!
}


void Wayfire::WM::ViewController::focus( Resource *, uint32_t uuid ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        wf::get_core().default_wm->focus_request( view );
    }
}


void Wayfire::WM::ViewController::maximize( Resource *, uint32_t uuid ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        /** First un-minimize the view if it was minimized */
        if ( view->minimized ) {
            wf::get_core().default_wm->minimize_request( view, false );
        }

        wf::get_core().default_wm->tile_request( view, wf::TILED_EDGES_ALL );
    }
}


void Wayfire::WM::ViewController::minimize( Resource *, uint32_t uuid ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        wf::get_core().default_wm->minimize_request( view, true );
    }
}


void Wayfire::WM::ViewController::restore( Resource *, uint32_t uuid ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        /** Ensure that this view is not fullscreened */
        if ( view->toplevel()->current().fullscreen ) {
            return;
        }

        /** This view was minimized */
        if ( view->minimized ) {
            /** Unminimize */
            wf::get_core().default_wm->minimize_request( view, false );
        }

        /** This view was tiled (i.e., maximized, or other tile) */
        else if ( view->toplevel()->current().tiled_edges ) {
            /** Unmaximize */
            wf::get_core().default_wm->tile_request( view, false );
        }
    }
}


void Wayfire::WM::ViewController::fullscreen( Resource *, uint32_t uuid, uint32_t op_uuid, uint32_t state ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        wf::output_t *output = get_output_from_id( op_uuid );

        wf::get_core().default_wm->fullscreen_request( view, output, ( bool )state );
    }
}


void Wayfire::WM::ViewController::pinAbove( Resource *, uint32_t uuid, uint32_t state ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        wf::wm_actions_set_above_state_signal set_on_top;
        set_on_top.view  = view;
        set_on_top.above = (bool)state;

        if ( view->get_output() ) {
            view->get_output()->emit( &set_on_top );
        }
    }
}


void Wayfire::WM::ViewController::pinBelow( Resource *, uint32_t, uint32_t ) {
    std::cerr << "This feature is not currently supported on wayfire" << std::endl;
}


void Wayfire::WM::ViewController::setSticky( Resource *, uint32_t uuid, uint32_t sticky ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        view->set_sticky( (bool)sticky );
    }
}


void Wayfire::WM::ViewController::setMinimizedGeometry( Resource *, uint32_t uuid, struct ::wl_resource *surfRes, int32_t x, int32_t y, int32_t w, int32_t h ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        auto surface = wf::wl_surface_to_wayfire_view( surfRes );

        if ( !surface ) {
            LOGE( "Setting minimize hint to unknown surface. Wayfire currently supports only setting hints relative to views." );
        }

        if ( surface->get_output() != view->get_output() ) {
            LOGE( "Minimize hint set to surface on a different output, problems might arise." );
            /* TODO: translate coordinates in case minimize hint is on another output */
        }

        wlr_box      hint { x, y, w, h };
        wf::pointf_t relative = surface->get_surface_root_node()->to_global( { 0, 0 } );
        hint.x += relative.x;
        hint.y += relative.y;
        view->set_minimize_hint( hint );
    }
}


void Wayfire::WM::ViewController::close( Resource *, uint32_t uuid ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        view->close();
    }
}


void Wayfire::WM::ViewController::kill( Resource *, uint32_t uuid, uint32_t signal ) {
    wayfire_toplevel_view view = get_view_from_id( uuid );

    if ( view ) {
        pid_t pid = 0;
        uid_t uid = 0;
        gid_t gid = 0;

        /** Get the view credentials */
        wl_client_get_credentials( view->get_client(), &pid, &uid, &gid );

        /** If we have a xwayland client, then over-ride the PID. */
        wlr_surface *wlr_surface = view->get_wlr_surface();

#if WF_HAS_XWAYLAND

        if ( auto xwayland_surface = wlr_xwayland_surface_try_from_wlr_surface( wlr_surface ) ) {
            pid = xwayland_surface->pid;
        }

#endif

        ::kill( pid, signal );
    }
}
