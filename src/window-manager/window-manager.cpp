/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 Marcus Britanicus
 * Implementation of the desq-shell-unstable-v1 protocol
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayland-server.h>
#include <wayfire/nonstd/wlroots-full.hpp>
#include <wayfire/util/log.hpp>

#include <memory>
#include <wayfire/core.hpp>
#include <wayfire/plugin.hpp>

#include "wayfire-view-controls-server.h"
#include "wayfire-view-controls-server.hpp"
#include "wayfire-view-controls.hpp"

#include "wayfire-view-lists-server.h"
#include "wayfire-view-lists-server.hpp"
#include "wayfire-view-lists.hpp"


namespace Wayfire {
    namespace WM {
        class ProtocolImpl;
    }
}


class Wayfire::WM::ProtocolImpl : public wf::plugin_interface_t {
    public:
        void init() override {
            LOGE( "INIT WINDOW MANAGER" );

            manager = new Wayfire::WM::ViewController( wf::get_core().display, 1 );
            wfLists = new Wayfire::WM::ViewLister( wf::get_core().display, 1 );
        }

        void fini() override {
            delete manager;
            delete wfLists;
        }

    private:
        Wayfire::WM::ViewController *manager = nullptr;
        Wayfire::WM::ViewLister     *wfLists = nullptr;
};

DECLARE_WAYFIRE_PLUGIN( Wayfire::WM::ProtocolImpl );
