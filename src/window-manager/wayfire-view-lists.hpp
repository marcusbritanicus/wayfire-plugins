/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 Marcus Britanicus
 * Implementation of the desq-shell-unstable-v1 protocol
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <wayland-server-core.h>
#include "wayfire-view-lists-server.hpp"

#include <map>
#include <string>

#include <wayfire/signal-definitions.hpp>
#include <wayfire/plugins/wm-actions-signals.hpp>

namespace Wayfire {
    namespace WM {
        class ViewLister;
        class View;
    }
}

enum WindowState {
    Active           = 1 << 0,
    Minimized        = 1 << 1,
    Maximized        = 1 << 2,
    Fullscreen       = 1 << 3,
    PinnedAbove      = 1 << 4,
    PinnedBelow      = 1 << 5,
    Sticky           = 1 << 6,
    DemandsAttention = 1 << 7,
};

class Wayfire::WM::ViewLister : public Wayland::Server::WayfireViewLister {
    public:
        ViewLister( struct ::wl_display *display, int version );
        virtual ~ViewLister();

    private:
        void watchForSignals();

        /** Various wayfire signal handlers */
        wf::signal::connection_t<wf::view_title_changed_signal> on_title_changed;
        wf::signal::connection_t<wf::view_app_id_changed_signal> on_app_id_changed;
        wf::signal::connection_t<wf::view_minimized_signal> on_minimized;
        wf::signal::connection_t<wf::view_fullscreen_signal> on_fullscreen;
        wf::signal::connection_t<wf::view_tiled_signal> on_tiled;
        wf::signal::connection_t<wf::view_activated_state_signal> on_activated;
        wf::signal::connection_t<wf::wm_actions_above_changed_signal> on_pinned_above;
        // wf::signal::connection_t<wf::wm_actions_below_changed_signal> on_pinned_below;
        wf::signal::connection_t<wf::view_set_sticky_signal> on_sticky;
        wf::signal::connection_t<wf::view_hints_changed_signal> on_view_hints;
        wf::signal::connection_t<wf::view_parent_changed_signal> on_parent_changed;

        /** To handle view_added and view_removed */
        wf::signal::connection_t<wf::output_added_signal> on_output_added;
        wf::signal::connection_t<wf::view_moved_to_wset_signal> on_view_moved_to_wset;
        wf::signal::connection_t<wf::view_change_workspace_signal> on_view_moved_to_workspace;

        /** To register other connection handlers */
        wf::signal::connection_t<wf::view_mapped_signal> on_view_mapped;

    protected:
        virtual void locateView( Resource *resource, uint32_t view );
};

// /****/
//     protected:
//         virtual Resource *allocate();
//
//         virtual void bindResource( Resource *resource );
//         virtual void destroyResource( Resource *resource );
//
//         virtual void locateView( Resource *resource, uint32_t view );
//         virtual void getTitle( Resource *resource, uint32_t view );
//         virtual void getAppId( Resource *resource, uint32_t view );
//         virtual void getState( Resource *resource, uint32_t view );
//         virtual void getParent( Resource *resource, uint32_t view );
//         virtual void getGeometry( Resource *resource, uint32_t view );
//         virtual void getProcessInfo( Resource *resource, uint32_t view );
//
//     private:
//         static void bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id );
//         static void destroy_func( struct ::wl_resource *client_resource );
//         static void display_destroy_func( struct ::wl_listener *listener, void *data );
//
//         Resource *bind( struct ::wl_client *client, uint32_t id, int version );
//         Resource *bind( struct ::wl_resource *handle );
//
//         static const struct ::wayfire_window_information_interface
// m_wayfire_window_information_interface;
//
//         static void handleLocateView( ::wl_client *client, struct wl_resource *resource, uint32_t view );
//         static void handleGetTitle( ::wl_client *client, struct wl_resource *resource, uint32_t view );
//         static void handleGetAppId( ::wl_client *client, struct wl_resource *resource, uint32_t view );
//         static void handleGetState( ::wl_client *client, struct wl_resource *resource, uint32_t view );
//         static void handleGetParent( ::wl_client *client, struct wl_resource *resource, uint32_t view );
//         static void handleGetGeometry( ::wl_client *client, struct wl_resource *resource, uint32_t view
// );
//         static void handleGetProcessInfo( ::wl_client *client, struct wl_resource *resource, uint32_t
// view );
//
//         std::multimap<struct ::wl_client *, Resource *> m_resource_map;
//
//         Resource *m_resource         = nullptr;
//         struct ::wl_global *m_global = nullptr;
//
//         uint32_t m_globalVersion;
//         struct DisplayDestroyedListener : ::wl_listener {
//             WayfireWindowInformation *parent;
//         };
//         DisplayDestroyedListener m_displayDestroyedListener;
//
//         /** Various wayfire signal handlers */
//         wf::signal::connection_t<wf::view_title_changed_signal> on_title_changed;
//         wf::signal::connection_t<wf::view_app_id_changed_signal> on_app_id_changed;
//         wf::signal::connection_t<wf::view_minimized_signal> on_minimized;
//         wf::signal::connection_t<wf::view_fullscreen_signal> on_fullscreen;
//         wf::signal::connection_t<wf::view_tiled_signal> on_tiled;
//         wf::signal::connection_t<wf::view_activated_state_signal> on_activated;
//         wf::signal::connection_t<wf::wm_actions_above_changed_signal> on_pinned_above;
//         // wf::signal::connection_t<wf::wm_actions_below_changed_signal> on_pinned_below;
//         wf::signal::connection_t<wf::view_set_sticky_signal> on_sticky;
//         wf::signal::connection_t<wf::view_hints_changed_signal> on_view_hints;
//         wf::signal::connection_t<wf::view_parent_changed_signal> on_parent_changed;
//
//         /** To handle view_added and view_removed */
//         wf::signal::connection_t<wf::output_added_signal> on_output_added;
//         wf::signal::connection_t<wf::view_moved_to_wset_signal> on_view_moved_to_wset;
//         wf::signal::connection_t<wf::view_change_workspace_signal> on_view_moved_to_workspace;
//
//         /** To register other connection handlers */
//         wf::signal::connection_t<wf::view_mapped_signal> on_view_mapped;
// };
