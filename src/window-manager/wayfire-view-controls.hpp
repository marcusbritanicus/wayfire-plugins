/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 Marcus Britanicus
 * Implementation of the desq-shell-unstable-v1 protocol
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include "wayfire-view-controls-server.hpp"

#include <map>
#include <string>

namespace Wayfire {
    namespace WM {
        class ViewController;
    }
}

class Wayfire::WM::ViewController : public Wayland::Server::WayfireViewController {
    public:
        ViewController( struct ::wl_display *display, int version );

        virtual void focus( Resource *resource, uint32_t view );
        virtual void maximize( Resource *resource, uint32_t view );
        virtual void minimize( Resource *resource, uint32_t view );
        virtual void restore( Resource *resource, uint32_t view );
        virtual void fullscreen( Resource *resource, uint32_t view, uint32_t output, uint32_t state );
        virtual void pinAbove( Resource *resource, uint32_t view, uint32_t state );
        virtual void pinBelow( Resource *resource, uint32_t view, uint32_t state );
        virtual void setSticky( Resource *resource, uint32_t view, uint32_t state );
        virtual void setMinimizedGeometry( Resource *resource, uint32_t view, struct ::wl_resource *surface, int32_t x, int32_t y, int32_t width, int32_t height );
        virtual void close( Resource *resource, uint32_t view );
        virtual void kill( Resource *resource, uint32_t view, uint32_t signal );
};
