// This file was generated by wayland-scribe v1.0.0
// Source: wayfire-view-controls.xml

#include <string>
#include "wayfire-view-controls-server.h"
#include "wayfire-view-controls-server.hpp"

Wayland::Server::WayfireViewController::WayfireViewController( struct ::wl_client *client, uint32_t id, int version ) {
    m_resource_map.clear();
    init( client, id, version );
}


Wayland::Server::WayfireViewController::WayfireViewController( struct ::wl_display *display, int version ) {
    m_resource_map.clear();
    init( display, version );
}


Wayland::Server::WayfireViewController::WayfireViewController( struct ::wl_resource *resource ) {
    m_resource_map.clear();
    init( resource );
}


Wayland::Server::WayfireViewController::WayfireViewController() {
    m_resource_map.clear();
}


Wayland::Server::WayfireViewController::~WayfireViewController() {
    for (auto it = m_resource_map.begin(); it != m_resource_map.end(); ) {
        Resource *resourcePtr = it->second;

        // Delete the Resource object pointed to by resourcePtr
        resourcePtr->wayfireViewControllerObject = nullptr;
    }

    if ( m_resource ) {
        m_resource->wayfireViewControllerObject = nullptr;
    }

    if ( m_global ) {
        wl_global_destroy( m_global );
        wl_list_remove( &m_displayDestroyedListener.link );
    }
}


void Wayland::Server::WayfireViewController::init( struct ::wl_client *client, uint32_t id, int version ) {
    m_resource = bind( client, id, version );
}


void Wayland::Server::WayfireViewController::init( struct ::wl_resource *resource ) {
    m_resource = bind( resource );
}


Wayland::Server::WayfireViewController::Resource *Wayland::Server::WayfireViewController::add( struct ::wl_client *client, int version ) {
    Resource *resource = bind( client, 0, version );

    std::cout << "-----------------> Adding client (controller)" << std::endl;

    m_resource_map.insert( std::pair { client, resource } );
    return resource;
}


Wayland::Server::WayfireViewController::Resource *Wayland::Server::WayfireViewController::add( struct ::wl_client *client, uint32_t id, int version ) {
    Resource *resource = bind( client, id, version );

    std::cout << "-----------------> Adding client (controller)" << std::endl;

    m_resource_map.insert( std::pair { client, resource } );
    return resource;
}


void Wayland::Server::WayfireViewController::init( struct ::wl_display *display, int version ) {
    m_global = wl_global_create( display, &::wayfire_view_controller_interface, version, this, bind_func );
    m_displayDestroyedListener.notify = WayfireViewController::display_destroy_func;
    m_displayDestroyedListener.parent = this;
    wl_display_add_destroy_listener( display, &m_displayDestroyedListener );
}


const struct wl_interface *Wayland::Server::WayfireViewController::interface() {
    return &::wayfire_view_controller_interface;
}


Wayland::Server::WayfireViewController::Resource *Wayland::Server::WayfireViewController::allocate() {
    return new Resource;
}


void Wayland::Server::WayfireViewController::bindResource( Resource * ) {
}


void Wayland::Server::WayfireViewController::destroyResource( Resource * ) {
}


void Wayland::Server::WayfireViewController::bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id ) {
    WayfireViewController *that = static_cast<WayfireViewController *>(data);

    that->add( client, id, version );
}


void Wayland::Server::WayfireViewController::display_destroy_func( struct ::wl_listener *listener, void * ) {
    WayfireViewController *that = static_cast<WayfireViewController::DisplayDestroyedListener *>(listener)->parent;

    that->m_global = nullptr;
}


void Wayland::Server::WayfireViewController::destroy_func( struct ::wl_resource *client_resource ) {
    Resource              *resource = Resource::fromResource( client_resource );
    WayfireViewController *that     = resource->wayfireViewControllerObject;

    if ( that ) {
        auto it = that->m_resource_map.begin();
        while ( it != that->m_resource_map.end() ) {
            if ( it->first == resource->client() ) {
                it = that->m_resource_map.erase( it );
            }

            else {
                ++it;
            }
        }
        that->destroyResource( resource );

        that = resource->wayfireViewControllerObject;

        if ( that && (that->m_resource == resource) ) {
            that->m_resource = nullptr;
        }
    }

    delete resource;
}


Wayland::Server::WayfireViewController::Resource *Wayland::Server::WayfireViewController::bind( struct ::wl_client *client, uint32_t id, int version ) {
    struct ::wl_resource *handle = wl_resource_create( client, &::wayfire_view_controller_interface, version, id );

    return bind( handle );
}


Wayland::Server::WayfireViewController::Resource *Wayland::Server::WayfireViewController::bind( struct ::wl_resource *handle ) {
    Resource *resource = allocate();

    resource->wayfireViewControllerObject = this;

    wl_resource_set_implementation( handle, &m_wayfire_view_controller_interface, resource, destroy_func );
    resource->handle = handle;
    bindResource( resource );
    return resource;
}


Wayland::Server::WayfireViewController::Resource *Wayland::Server::WayfireViewController::Resource::fromResource( struct ::wl_resource *resource ) {
    if ( !resource ) {
        return nullptr;
    }

    if ( wl_resource_instance_of( resource, &::wayfire_view_controller_interface, &m_wayfire_view_controller_interface ) ) {
        return static_cast<Resource *>(wl_resource_get_user_data( resource ) );
    }

    return nullptr;
}


const struct ::wayfire_view_controller_interface Wayland::Server::WayfireViewController::m_wayfire_view_controller_interface = {
    Wayland::Server::WayfireViewController::handleFocus,
    Wayland::Server::WayfireViewController::handleMaximize,
    Wayland::Server::WayfireViewController::handleMinimize,
    Wayland::Server::WayfireViewController::handleRestore,
    Wayland::Server::WayfireViewController::handleFullscreen,
    Wayland::Server::WayfireViewController::handlePinAbove,
    Wayland::Server::WayfireViewController::handlePinBelow,
    Wayland::Server::WayfireViewController::handleSetSticky,
    Wayland::Server::WayfireViewController::handleSetMinimizedGeometry,
    Wayland::Server::WayfireViewController::handleClose,
    Wayland::Server::WayfireViewController::handleKill
};

void Wayland::Server::WayfireViewController::focus( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireViewController::maximize( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireViewController::minimize( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireViewController::restore( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireViewController::fullscreen( Resource *, uint32_t, uint32_t, uint32_t ) {
}


void Wayland::Server::WayfireViewController::pinAbove( Resource *, uint32_t, uint32_t ) {
}


void Wayland::Server::WayfireViewController::pinBelow( Resource *, uint32_t, uint32_t ) {
}


void Wayland::Server::WayfireViewController::setSticky( Resource *, uint32_t, uint32_t ) {
}


void Wayland::Server::WayfireViewController::setMinimizedGeometry( Resource *, uint32_t, struct ::wl_resource *, int32_t, int32_t, int32_t, int32_t ) {
}


void Wayland::Server::WayfireViewController::close( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireViewController::kill( Resource *, uint32_t, uint32_t ) {
}


void Wayland::Server::WayfireViewController::handleFocus( ::wl_client *, struct wl_resource *resource, uint32_t view ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->focus( r, view );
}


void Wayland::Server::WayfireViewController::handleMaximize( ::wl_client *, struct wl_resource *resource, uint32_t view ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->maximize( r, view );
}


void Wayland::Server::WayfireViewController::handleMinimize( ::wl_client *, struct wl_resource *resource, uint32_t view ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->minimize( r, view );
}


void Wayland::Server::WayfireViewController::handleRestore( ::wl_client *, struct wl_resource *resource, uint32_t view ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->restore( r, view );
}


void Wayland::Server::WayfireViewController::handleFullscreen( ::wl_client *, struct wl_resource *resource, uint32_t view, uint32_t output, uint32_t state ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->fullscreen( r, view, output, state );
}


void Wayland::Server::WayfireViewController::handlePinAbove( ::wl_client *, struct wl_resource *resource, uint32_t view, uint32_t state ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->pinAbove( r, view, state );
}


void Wayland::Server::WayfireViewController::handlePinBelow( ::wl_client *, struct wl_resource *resource, uint32_t view, uint32_t state ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->pinBelow( r, view, state );
}


void Wayland::Server::WayfireViewController::handleSetSticky( ::wl_client *, struct wl_resource *resource, uint32_t view, uint32_t state ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->setSticky( r, view, state );
}


void Wayland::Server::WayfireViewController::handleSetMinimizedGeometry( ::wl_client *, struct wl_resource *resource, uint32_t view, struct ::wl_resource *surface, int32_t x, int32_t y, int32_t width, int32_t height ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->setMinimizedGeometry( r, view, surface, x, y, width, height );
}


void Wayland::Server::WayfireViewController::handleClose( ::wl_client *, struct wl_resource *resource, uint32_t view ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->close( r, view );
}


void Wayland::Server::WayfireViewController::handleKill( ::wl_client *, struct wl_resource *resource, uint32_t view, uint32_t signal ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireViewControllerObject ) {
        return;
    }

    static_cast<WayfireViewController *>(r->wayfireViewControllerObject)->kill( r, view, signal );
}
