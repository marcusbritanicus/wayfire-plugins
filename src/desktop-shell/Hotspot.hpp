/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <wayland-client.h>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/nonstd/wlroots-full.hpp>

/**
 * Represents a zdesq_shell_hotspot_v1.
 * Lifetime is managed by the resource.
 */

#include "wayland-server-core.h"
#include "wayfire-desktop-shell.h"
#include <map>
#include <string>

namespace Wayland {
    namespace Server {
        class WayfireHotspot;
    }
}

namespace Wayfire {
    namespace DesktopShell {
        class Hotspot;
    }
}


class Wayland::Server::WayfireHotspot {
    public:
        WayfireHotspot( wf::output_t *output, uint32_t edge_mask, uint32_t distance, uint32_t timeout, struct ::wl_client *client, int id, int version );
        virtual ~WayfireHotspot();

        class Resource {
            public:
                Resource() : WayfireHotspotObject( nullptr ), handle( nullptr ) {}
                virtual ~Resource() {}

                WayfireHotspot *WayfireHotspotObject;
                WayfireHotspot *object() { return WayfireHotspotObject; }
                struct ::wl_resource *handle;

                struct ::wl_client *client() const { return wl_resource_get_client( handle ); }
                int version() const { return wl_resource_get_version( handle ); }

                static Resource *fromResource( struct ::wl_resource *resource );
        };

        void init( struct ::wl_client *client, int id, int version );
        Resource *add( struct ::wl_client *client, int id, int version );

        Resource *resource() { return m_resource; }
        const Resource *resource() const { return m_resource; }

        std::multimap<struct ::wl_client *, Resource *> resourceMap() { return m_resource_map; }
        const std::multimap<struct ::wl_client *, Resource *> resourceMap() const { return m_resource_map; }

        bool isGlobal() const { return m_global != nullptr; }
        bool isResource() const { return m_resource != nullptr; }

        static const struct ::wl_interface *interface();

        static std::string interfaceName() { return interface()->name; }
        static int interfaceVersion() { return interface()->version; }

        void sendEnter();
        void sendEnter( struct ::wl_resource *resource );
        void sendLeave();
        void sendLeave( struct ::wl_resource *resource );

    protected:
        virtual Resource *allocate();

        virtual void bindResource( Resource *resource );
        virtual void destroyResource( Resource *resource );

    private:
        static void bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id );
        static void destroy_func( struct ::wl_resource *client_resource );
        static void display_destroy_func( struct ::wl_listener *listener, void *data );

        Resource *bind( struct ::wl_client *client, uint32_t id, int version );
        Resource *bind( struct ::wl_resource *handle );

        std::multimap<struct ::wl_client *, Resource *> m_resource_map;
        Resource *m_resource         = nullptr;
        struct ::wl_global *m_global = nullptr;
        uint32_t m_globalVersion;
        struct DisplayDestroyedListener : ::wl_listener {
            WayfireHotspot *parent;
        };
        DisplayDestroyedListener m_displayDestroyedListener;

        /** Wayfire-related varaibles and functions */
        wf::geometry_t hotspot_geometry;

        bool hotspot_triggered = false;
        wf::wl_idle_call idle_check_input;
        wf::wl_timer<false> timer;

        uint32_t timeout_ms;
        wl_resource *hotspot_resource;

        wf::signal::connection_t<wf::post_input_event_signal<wlr_tablet_tool_axis_event> > on_tablet_axis;
        wf::signal::connection_t<wf::post_input_event_signal<wlr_pointer_motion_event> > on_motion_event;
        wf::signal::connection_t<wf::post_input_event_signal<wlr_touch_motion_event> > on_touch_motion;
        wf::signal::connection_t<wf::output_removed_signal> on_output_removed;

        /** Connect Mouse/touch events to handlers */
        void initConnections();

        /** How do we process the input motion */
        void processInputMotion( wf::point_t gc );

        /** Calculate the geometry of the hotspot for a given output */
        wf::geometry_t calculateHotspotGeometry( wf::output_t *output, uint32_t edge_mask, uint32_t distance ) const;
};
