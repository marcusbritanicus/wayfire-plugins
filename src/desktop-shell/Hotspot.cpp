/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayland-client.h>
#include <wayfire/output.hpp>
#include <wayfire/core.hpp>
#include <wayfire/output-layout.hpp>

#include "Hotspot.hpp"


Wayland::Server::WayfireHotspot::WayfireHotspot( wf::output_t *output, uint32_t edge_mask, uint32_t distance, uint32_t timeout, struct ::wl_client *client, int id, int version ) {
    m_resource_map.clear();
    init( client, id, version );

    initConnections();

    this->timeout_ms       = timeout;
    this->hotspot_geometry = calculateHotspotGeometry( output, edge_mask, distance );

    // setup output destroy listener
    on_output_removed.set_callback(
        [ this, output ] (wf::output_removed_signal *ev){
            if ( ev->output == output ) {
                /* Make hotspot inactive by setting the region to empty */
                hotspot_geometry = { 0, 0, 0, 0 };
                processInputMotion( { 0, 0 } );
            }
        }
    );

    wf::get_core().connect( &on_motion_event );
    wf::get_core().connect( &on_touch_motion );
    wf::get_core().connect( &on_tablet_axis );
    wf::get_core().output_layout->connect( &on_output_removed );
}


Wayland::Server::WayfireHotspot::~WayfireHotspot() {
    for (auto it = m_resource_map.begin(); it != m_resource_map.end(); ) {
        Resource *resourcePtr = it->second;

        // Delete the Resource object pointed to by resourcePtr
        resourcePtr->WayfireHotspotObject = nullptr;
    }

    if ( m_resource ) {
        m_resource->WayfireHotspotObject = nullptr;
    }

    if ( m_global ) {
        wl_global_destroy( m_global );
        wl_list_remove( &m_displayDestroyedListener.link );
    }
}


Wayland::Server::WayfireHotspot::Resource *Wayland::Server::WayfireHotspot::add( struct ::wl_client *client, int id, int version ) {
    Resource *resource = bind( client, id, version );

    m_resource_map.insert( std::pair { client, resource } );
    return resource;
}


void Wayland::Server::WayfireHotspot::init( struct ::wl_client *client, int id, int version ) {
    m_resource = bind( client, id, version );
}


const struct wl_interface *Wayland::Server::WayfireHotspot::interface() {
    return &::wayfire_hotspot_interface;
}


Wayland::Server::WayfireHotspot::Resource *Wayland::Server::WayfireHotspot::allocate() {
    return new Resource;
}


void Wayland::Server::WayfireHotspot::bindResource( Resource * ) {
}


void Wayland::Server::WayfireHotspot::destroyResource( Resource * ) {
}


void Wayland::Server::WayfireHotspot::bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id ) {
    WayfireHotspot *that = static_cast<WayfireHotspot *>(data);

    that->add( client, id, std::min( that->m_globalVersion, version ) );
}


void Wayland::Server::WayfireHotspot::display_destroy_func( struct ::wl_listener *listener, void * ) {
    // Q_UNUSED( data );
    WayfireHotspot *that = static_cast<Wayland::Server::WayfireHotspot::DisplayDestroyedListener *>(listener)->parent;

    that->m_global = nullptr;
}


void Wayland::Server::WayfireHotspot::destroy_func( struct ::wl_resource *client_resource ) {
    Resource *resource = Resource::fromResource( client_resource );

    WayfireHotspot *that = resource->WayfireHotspotObject;

    if ( that ) {
        auto it = that->m_resource_map.begin();
        while ( it != that->m_resource_map.end() ) {
            if ( it->first == resource->client() ) {
                it = that->m_resource_map.erase( it );
            }

            else {
                ++it;
            }
        }

        that->destroyResource( resource );

        that = resource->WayfireHotspotObject;

        if ( that && (that->m_resource == resource) ) {
            that->m_resource = nullptr;
        }
    }

    delete resource;
}


Wayland::Server::WayfireHotspot::Resource *Wayland::Server::WayfireHotspot::bind( struct ::wl_client *client, uint32_t id, int version ) {
    struct ::wl_resource *handle = wl_resource_create( client, &::wayfire_hotspot_interface, version, id );

    return bind( handle );
}


Wayland::Server::WayfireHotspot::Resource *Wayland::Server::WayfireHotspot::bind( struct ::wl_resource *handle ) {
    Resource *resource = allocate();

    resource->WayfireHotspotObject = this;

    wl_resource_set_implementation( handle, nullptr, resource, destroy_func );
    resource->handle = handle;
    bindResource( resource );
    return resource;
}


Wayland::Server::WayfireHotspot::Resource *Wayland::Server::WayfireHotspot::Resource::fromResource( struct ::wl_resource *resource ) {
    if ( !resource ) {
        return nullptr;
    }

    if ( wl_resource_instance_of( resource, &::wayfire_hotspot_interface, nullptr ) ) {
        return static_cast<Resource *>(wl_resource_get_user_data( resource ) );
    }

    return nullptr;
}


void Wayland::Server::WayfireHotspot::sendEnter() {
    if ( !m_resource ) {
        return;
    }

    sendEnter( m_resource->handle );
}


void Wayland::Server::WayfireHotspot::sendEnter( struct ::wl_resource *resource ) {
    wayfire_hotspot_send_enter( resource );
}


void Wayland::Server::WayfireHotspot::sendLeave() {
    if ( !m_resource ) {
        return;
    }

    sendLeave( m_resource->handle );
}


void Wayland::Server::WayfireHotspot::sendLeave( struct ::wl_resource *resource ) {
    wayfire_hotspot_send_leave( resource );
}


/**
 * Represents wayfire_hotspot.
 * Lifetime is managed by the resource.
 */
void Wayland::Server::WayfireHotspot::initConnections() {
    on_tablet_axis =
        [ = ] (auto){
            idle_check_input.run_once(
                [ = ] () {
                    auto gcf = wf::get_core().get_cursor_position();
                    wf::point_t gc { (int)gcf.x, (int)gcf.y };
                    processInputMotion( gc );
                }
            );
        };

    on_motion_event =
        [ = ] (auto){
            idle_check_input.run_once(
                [ = ] () {
                    auto gcf = wf::get_core().get_cursor_position();
                    wf::point_t gc { (int)gcf.x, (int)gcf.y };
                    processInputMotion( gc );
                }
            );
        };

    on_touch_motion =
        [ = ] (auto) {
            idle_check_input.run_once(
                [ = ] () {
                    auto gcf = wf::get_core().get_touch_position( 0 );
                    wf::point_t gc { (int)gcf.x, (int)gcf.y };
                    processInputMotion( gc );
                }
            );
        };
}


void Wayland::Server::WayfireHotspot::processInputMotion( wf::point_t gc ) {
    if ( !(hotspot_geometry & gc) ) {
        if ( hotspot_triggered ) {
            sendLeave();
        }

        /* Cursor outside of the hotspot */
        hotspot_triggered = false;
        timer.disconnect();

        return;
    }

    if ( hotspot_triggered ) {
        /* Hotspot was already triggered, wait for the next time the cursor
         * enters the hotspot area to trigger again */
        return;
    }

    if ( !timer.is_connected() ) {
        timer.set_timeout(
            timeout_ms, [ = ] () {
                hotspot_triggered = true;
                sendEnter();
                return false;
            }
        );
    }
}


wf::geometry_t Wayland::Server::WayfireHotspot::calculateHotspotGeometry( wf::output_t *output, uint32_t edge_mask, uint32_t distance ) const {
    wf::geometry_t slot = output->get_layout_geometry();

    if ( edge_mask & WAYFIRE_OUTPUT_HOTSPOT_EDGE_TOP ) {
        slot.height = distance;
    }

    else if ( edge_mask & WAYFIRE_OUTPUT_HOTSPOT_EDGE_BOTTOM ) {
        slot.y     += slot.height - distance;
        slot.height = distance;
    }

    if ( edge_mask & WAYFIRE_OUTPUT_HOTSPOT_EDGE_LEFT ) {
        slot.width = distance;
    }

    else if ( edge_mask & WAYFIRE_OUTPUT_HOTSPOT_EDGE_RIGHT ) {
        slot.x    += slot.width - distance;
        slot.width = distance;
    }

    return slot;
}
