/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 Marcus Britanicus
 * Implementation of the desq-shell-unstable-v1 protocol
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "wayfire-desktop-shell.h"
#include "wayfire-desktop-shell.hpp"

#include <wayfire/core.hpp>
#include <wayfire/seat.hpp>
#include <wayfire/output-layout.hpp>

#include "Output.hpp"
#include "Hotspot.hpp"

#include <iostream>
#include <utility>

Wayland::Server::WayfireDesktopShell::WayfireDesktopShell( struct ::wl_display *display, int version ) {
    m_resource_map.clear();
    init( display, version );
}


Wayland::Server::WayfireDesktopShell::~WayfireDesktopShell() {
    for (auto it = m_resource_map.begin(); it != m_resource_map.end(); ) {
        Resource *resourcePtr = it->second;

        // Delete the Resource object pointed to by resourcePtr
        resourcePtr->WayfireDesktopShellObject = nullptr;
    }

    if ( m_resource ) {
        m_resource->WayfireDesktopShellObject = nullptr;
    }

    if ( m_global ) {
        wl_global_destroy( m_global );
        wl_list_remove( &m_displayDestroyedListener.link );
    }
}


Wayland::Server::WayfireDesktopShell::Resource *Wayland::Server::WayfireDesktopShell::add( struct ::wl_client *client, int id, int version ) {
    Resource *resource = bind( client, id, version );

    m_resource_map.insert( std::pair { client, resource } );
    return resource;
}


void Wayland::Server::WayfireDesktopShell::init( struct ::wl_display *display, int version ) {
    m_global        = wl_global_create( display, &::wayfire_desktop_shell_interface, version, this, bind_func );
    m_globalVersion = version;
    m_displayDestroyedListener.notify = Wayland::Server::WayfireDesktopShell::display_destroy_func;
    m_displayDestroyedListener.parent = this;
    wl_display_add_destroy_listener( display, &m_displayDestroyedListener );
}


const struct wl_interface *Wayland::Server::WayfireDesktopShell::interface() {
    return &::wayfire_desktop_shell_interface;
}


Wayland::Server::WayfireDesktopShell::Resource *Wayland::Server::WayfireDesktopShell::allocate() {
    return new Resource;
}


void Wayland::Server::WayfireDesktopShell::bindResource( Resource * ) {
}


void Wayland::Server::WayfireDesktopShell::destroyResource( Resource * ) {
}


void Wayland::Server::WayfireDesktopShell::bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id ) {
    WayfireDesktopShell *that = static_cast<WayfireDesktopShell *>(data);

    that->add( client, id, std::min( that->m_globalVersion, version ) );
}


void Wayland::Server::WayfireDesktopShell::display_destroy_func( struct ::wl_listener *listener, void * ) {
    // Q_UNUSED( data );
    WayfireDesktopShell *that = static_cast<Wayland::Server::WayfireDesktopShell::DisplayDestroyedListener *>(listener)->parent;

    that->m_global = nullptr;
}


void Wayland::Server::WayfireDesktopShell::destroy_func( struct ::wl_resource *client_resource ) {
    Resource *resource = Resource::fromResource( client_resource );

    WayfireDesktopShell *that = resource->WayfireDesktopShellObject;

    if ( that ) {
        auto it = that->m_resource_map.begin();
        while ( it != that->m_resource_map.end() ) {
            if ( it->first == resource->client() ) {
                it = that->m_resource_map.erase( it );
            }

            else {
                ++it;
            }
        }

        that->destroyResource( resource );

        that = resource->WayfireDesktopShellObject;

        if ( that && (that->m_resource == resource) ) {
            that->m_resource = nullptr;
        }
    }

    delete resource;
}


Wayland::Server::WayfireDesktopShell::Resource *Wayland::Server::WayfireDesktopShell::bind( struct ::wl_client *client, uint32_t id, int version ) {
    struct ::wl_resource *handle = wl_resource_create( client, &::wayfire_desktop_shell_interface, version, id );

    return bind( handle );
}


Wayland::Server::WayfireDesktopShell::Resource *Wayland::Server::WayfireDesktopShell::bind( struct ::wl_resource *handle ) {
    Resource *resource = allocate();

    resource->WayfireDesktopShellObject = this;

    wl_resource_set_implementation( handle, &m_wayfire_desktop_shell_interface, resource, destroy_func );
    resource->handle = handle;
    bindResource( resource );
    return resource;
}


Wayland::Server::WayfireDesktopShell::Resource *Wayland::Server::WayfireDesktopShell::Resource::fromResource( struct ::wl_resource *resource ) {
    if ( !resource ) {
        return nullptr;
    }

    if ( wl_resource_instance_of( resource, &::wayfire_desktop_shell_interface, &m_wayfire_desktop_shell_interface ) ) {
        return static_cast<Resource *>(wl_resource_get_user_data( resource ) );
    }

    return nullptr;
}


const struct ::wayfire_desktop_shell_interface Wayland::Server::WayfireDesktopShell::m_wayfire_desktop_shell_interface = {
    Wayland::Server::WayfireDesktopShell::handleCreateWayfireOutput,
    Wayland::Server::WayfireDesktopShell::handleCreateWayfireShellSurface,
    Wayland::Server::WayfireDesktopShell::handleSetAsBackground,
    Wayland::Server::WayfireDesktopShell::handleSetAsPanel,
    Wayland::Server::WayfireDesktopShell::handleSetAsDropdown,
    Wayland::Server::WayfireDesktopShell::handleSetAsNotification
};

void Wayland::Server::WayfireDesktopShell::createWayfireOutput( Resource *resource, struct ::wl_resource *output, uint32_t id ) {
    auto wlr_out = (wlr_output *)wl_resource_get_user_data( output );
    auto wo      = wf::get_core().output_layout->find_output( wlr_out );

    if ( wo ) {
        // will be deleted when the resource is destroyed
        new Wayland::Server::WayfireOutput( wo, wl_resource_get_client( resource->handle ), id, 1 );
    }
}


void Wayland::Server::WayfireDesktopShell::createWayfireShellSurface( Resource *, struct ::wl_resource *, uint32_t ) {
}


void Wayland::Server::WayfireDesktopShell::setAsBackground( Resource *, struct ::wl_resource *, struct ::wl_resource *, uint32_t ) {
}


void Wayland::Server::WayfireDesktopShell::setAsPanel( Resource *, struct ::wl_resource *, struct ::wl_resource *, uint32_t, uint32_t ) {
}


void Wayland::Server::WayfireDesktopShell::setAsDropdown( Resource *, struct ::wl_resource *, uint32_t ) {
}


void Wayland::Server::WayfireDesktopShell::setAsNotification( Resource *, struct ::wl_resource *, uint32_t ) {
}


void Wayland::Server::WayfireDesktopShell::handleCreateWayfireOutput( ::wl_client *, struct wl_resource *resource, struct ::wl_resource *output, uint32_t id ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireDesktopShellObject ) {
        return;
    }

    static_cast<WayfireDesktopShell *>(r->WayfireDesktopShellObject)->createWayfireOutput(
        r,
        output,
        id );
}


void Wayland::Server::WayfireDesktopShell::handleCreateWayfireShellSurface( ::wl_client *, struct wl_resource *resource, struct ::wl_resource *surface, uint32_t id ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireDesktopShellObject ) {
        return;
    }

    static_cast<WayfireDesktopShell *>(r->WayfireDesktopShellObject)->createWayfireShellSurface(
        r,
        surface,
        id );
}


void Wayland::Server::WayfireDesktopShell::handleSetAsBackground( ::wl_client *, struct wl_resource *resource, struct ::wl_resource *surface, struct ::wl_resource *output, uint32_t id ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireDesktopShellObject ) {
        return;
    }

    static_cast<WayfireDesktopShell *>(r->WayfireDesktopShellObject)->setAsBackground(
        r,
        surface,
        output,
        id );
}


void Wayland::Server::WayfireDesktopShell::handleSetAsPanel( ::wl_client *, struct wl_resource *resource, struct ::wl_resource *surface, struct ::wl_resource *output, uint32_t anchor, uint32_t id ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireDesktopShellObject ) {
        return;
    }

    static_cast<WayfireDesktopShell *>(r->WayfireDesktopShellObject)->setAsPanel(
        r,
        surface,
        output,
        anchor,
        id );
}


void Wayland::Server::WayfireDesktopShell::handleSetAsDropdown( ::wl_client *, struct wl_resource *resource, struct ::wl_resource *surface, uint32_t id ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireDesktopShellObject ) {
        return;
    }

    static_cast<WayfireDesktopShell *>(r->WayfireDesktopShellObject)->setAsDropdown(
        r,
        surface,
        id );
}


void Wayland::Server::WayfireDesktopShell::handleSetAsNotification( ::wl_client *, struct wl_resource *resource, struct ::wl_resource *surface, uint32_t id ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireDesktopShellObject ) {
        return;
    }

    static_cast<WayfireDesktopShell *>(r->WayfireDesktopShellObject)->setAsNotification(
        r,
        surface,
        id );
}


Wayland::Server::WayfireShellSurface::WayfireShellSurface( struct ::wl_display *display, int version ) {
    m_resource_map.clear();
    init( display, version );
}


Wayland::Server::WayfireShellSurface::~WayfireShellSurface() {
    for (auto it = m_resource_map.begin(); it != m_resource_map.end(); ) {
        Resource *resourcePtr = it->second;

        // Delete the Resource object pointed to by resourcePtr
        resourcePtr->WayfireShellSurfaceObject = nullptr;
    }

    if ( m_resource ) {
        m_resource->WayfireShellSurfaceObject = nullptr;
    }

    if ( m_global ) {
        wl_global_destroy( m_global );
        wl_list_remove( &m_displayDestroyedListener.link );
    }
}


Wayland::Server::WayfireShellSurface::Resource *Wayland::Server::WayfireShellSurface::add( struct ::wl_client *client, int id, int version ) {
    Resource *resource = bind( client, id, version );

    m_resource_map.insert( std::pair { client, resource } );
    return resource;
}


void Wayland::Server::WayfireShellSurface::init( struct ::wl_display *display, int version ) {
    m_global        = wl_global_create( display, &::wayfire_shell_surface_interface, version, this, bind_func );
    m_globalVersion = version;
    m_displayDestroyedListener.notify = Wayland::Server::WayfireShellSurface::display_destroy_func;
    m_displayDestroyedListener.parent = this;
    wl_display_add_destroy_listener( display, &m_displayDestroyedListener );
}


const struct wl_interface *Wayland::Server::WayfireShellSurface::interface() {
    return &::wayfire_shell_surface_interface;
}


Wayland::Server::WayfireShellSurface::Resource *Wayland::Server::WayfireShellSurface::allocate() {
    return new Resource;
}


void Wayland::Server::WayfireShellSurface::bindResource( Resource * ) {
}


void Wayland::Server::WayfireShellSurface::destroyResource( Resource * ) {
}


void Wayland::Server::WayfireShellSurface::bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id ) {
    WayfireShellSurface *that = static_cast<WayfireShellSurface *>(data);

    that->add( client, id, std::min( that->m_globalVersion, version ) );
}


void Wayland::Server::WayfireShellSurface::display_destroy_func( struct ::wl_listener *listener, void * ) {
    // Q_UNUSED( data );
    WayfireShellSurface *that = static_cast<Wayland::Server::WayfireShellSurface::DisplayDestroyedListener *>(listener)->parent;

    that->m_global = nullptr;
}


void Wayland::Server::WayfireShellSurface::destroy_func( struct ::wl_resource *client_resource ) {
    Resource *resource = Resource::fromResource( client_resource );

    WayfireShellSurface *that = resource->WayfireShellSurfaceObject;

    if ( that ) {
        auto it = that->m_resource_map.begin();
        while ( it != that->m_resource_map.end() ) {
            if ( it->first == resource->client() ) {
                it = that->m_resource_map.erase( it );
            }

            else {
                ++it;
            }
        }

        that->destroyResource( resource );

        that = resource->WayfireShellSurfaceObject;

        if ( that && (that->m_resource == resource) ) {
            that->m_resource = nullptr;
        }
    }

    delete resource;
}


Wayland::Server::WayfireShellSurface::Resource *Wayland::Server::WayfireShellSurface::bind( struct ::wl_client *client, uint32_t id, int version ) {
    struct ::wl_resource *handle = wl_resource_create( client, &::wayfire_shell_surface_interface, version, id );

    return bind( handle );
}


Wayland::Server::WayfireShellSurface::Resource *Wayland::Server::WayfireShellSurface::bind( struct ::wl_resource *handle ) {
    Resource *resource = allocate();

    resource->WayfireShellSurfaceObject = this;

    wl_resource_set_implementation( handle, &m_wayfire_shell_surface_interface, resource, destroy_func );
    resource->handle = handle;
    bindResource( resource );
    return resource;
}


Wayland::Server::WayfireShellSurface::Resource *Wayland::Server::WayfireShellSurface::Resource::fromResource( struct ::wl_resource *resource ) {
    if ( !resource ) {
        return nullptr;
    }

    if ( wl_resource_instance_of( resource, &::wayfire_shell_surface_interface, &m_wayfire_shell_surface_interface ) ) {
        return static_cast<Resource *>(wl_resource_get_user_data( resource ) );
    }

    return nullptr;
}


const struct ::wayfire_shell_surface_interface Wayland::Server::WayfireShellSurface::m_wayfire_shell_surface_interface =
{
    Wayland::Server::WayfireShellSurface::handleSetLayer,
    Wayland::Server::WayfireShellSurface::handleSetAnchor,
    Wayland::Server::WayfireShellSurface::handleSetKeyboardInteraction,
    Wayland::Server::WayfireShellSurface::handleSetMouseInteraction,
    Wayland::Server::WayfireShellSurface::handleSetExclusiveZone,
    Wayland::Server::WayfireShellSurface::handleSetMargins
};

void Wayland::Server::WayfireShellSurface::setLayer( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireShellSurface::setAnchor( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireShellSurface::setKeyboardInteraction( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireShellSurface::setMouseInteraction( Resource *, uint32_t ) {
}


void Wayland::Server::WayfireShellSurface::setExclusiveZone( Resource *, int32_t ) {
}


void Wayland::Server::WayfireShellSurface::setMargins( Resource *, int32_t, int32_t, int32_t, int32_t ) {
}


void Wayland::Server::WayfireShellSurface::handleSetLayer( ::wl_client *, struct wl_resource *resource, uint32_t layer ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireShellSurfaceObject ) {
        return;
    }

    static_cast<WayfireShellSurface *>(r->WayfireShellSurfaceObject)->setLayer( r, layer );
}


void Wayland::Server::WayfireShellSurface::handleSetAnchor( ::wl_client *, struct wl_resource *resource, uint32_t edges ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireShellSurfaceObject ) {
        return;
    }

    static_cast<WayfireShellSurface *>(r->WayfireShellSurfaceObject)->setAnchor( r, edges );
}


void Wayland::Server::WayfireShellSurface::handleSetKeyboardInteraction( ::wl_client *, struct wl_resource *resource, uint32_t strength ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireShellSurfaceObject ) {
        return;
    }

    static_cast<WayfireShellSurface *>(r->WayfireShellSurfaceObject)->setKeyboardInteraction( r, strength );
}


void Wayland::Server::WayfireShellSurface::handleSetMouseInteraction( ::wl_client *, struct wl_resource *resource, uint32_t strength ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireShellSurfaceObject ) {
        return;
    }

    static_cast<WayfireShellSurface *>(r->WayfireShellSurfaceObject)->setMouseInteraction( r, strength );
}


void Wayland::Server::WayfireShellSurface::handleSetExclusiveZone( ::wl_client *, struct wl_resource *resource, int32_t zone ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireShellSurfaceObject ) {
        return;
    }

    static_cast<WayfireShellSurface *>(r->WayfireShellSurfaceObject)->setExclusiveZone( r, zone );
}


void Wayland::Server::WayfireShellSurface::handleSetMargins( ::wl_client *, struct wl_resource *resource, int32_t top, int32_t right, int32_t bottom, int32_t left ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->WayfireShellSurfaceObject ) {
        return;
    }

    static_cast<WayfireShellSurface *>(r->WayfireShellSurfaceObject)->setMargins( r, top, right, bottom, left );
}
