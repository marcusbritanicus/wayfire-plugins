/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 Marcus Britanicus
 * Implementation of the desq-shell-unstable-v1 protocol
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include "wayland-server-core.h"
#include "wayfire-desktop-shell.h"
#include <map>
#include <string>

namespace Wayland {
    namespace Server {
        class WayfireDesktopShell;
        class WayfireOutput;
        class WayfireShellSurface;
    }
}

class Wayland::Server::WayfireDesktopShell {
    public:
        WayfireDesktopShell( struct ::wl_display *display, int version );
        virtual ~WayfireDesktopShell();

        class Resource {
            public:
                Resource() : WayfireDesktopShellObject( nullptr ), handle( nullptr ) {}
                virtual ~Resource() {}

                WayfireDesktopShell *WayfireDesktopShellObject;
                WayfireDesktopShell *object() { return WayfireDesktopShellObject; }
                struct ::wl_resource *handle;

                struct ::wl_client *client() const { return wl_resource_get_client( handle ); }
                int version() const { return wl_resource_get_version( handle ); }

                static Resource *fromResource( struct ::wl_resource *resource );
        };

        void init( struct ::wl_display *display, int version );
        Resource *add( struct ::wl_client *client, int id, int version );

        Resource *resource() { return m_resource; }
        const Resource *resource() const { return m_resource; }

        std::multimap<struct ::wl_client *, Resource *> resourceMap() { return m_resource_map; }
        const std::multimap<struct ::wl_client *, Resource *> resourceMap() const { return m_resource_map; }

        bool isGlobal() const { return m_global != nullptr; }
        bool isResource() const { return m_resource != nullptr; }

        static const struct ::wl_interface *interface();

        static std::string interfaceName() { return interface()->name; }
        static int interfaceVersion() { return interface()->version; }


    protected:
        virtual Resource *allocate();

        virtual void bindResource( Resource *resource );
        virtual void destroyResource( Resource *resource );

        virtual void createWayfireOutput( Resource *resource, struct ::wl_resource *output, uint32_t id );
        virtual void createWayfireShellSurface( Resource *resource, struct ::wl_resource *surface, uint32_t id );
        virtual void setAsBackground( Resource *resource, struct ::wl_resource *surface, struct ::wl_resource *output, uint32_t id );
        virtual void setAsPanel( Resource *resource, struct ::wl_resource *surface, struct ::wl_resource *output, uint32_t anchor, uint32_t id );
        virtual void setAsDropdown( Resource *resource, struct ::wl_resource *surface, uint32_t id );
        virtual void setAsNotification( Resource *resource, struct ::wl_resource *surface, uint32_t id );

    private:
        static void bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id );
        static void destroy_func( struct ::wl_resource *client_resource );
        static void display_destroy_func( struct ::wl_listener *listener, void *data );

        Resource *bind( struct ::wl_client *client, uint32_t id, int version );
        Resource *bind( struct ::wl_resource *handle );

        static const struct ::wayfire_desktop_shell_interface m_wayfire_desktop_shell_interface;

        static void handleCreateWayfireOutput( ::wl_client *client, struct wl_resource *resource, struct ::wl_resource *output, uint32_t id );
        static void handleCreateWayfireShellSurface( ::wl_client *client, struct wl_resource *resource, struct ::wl_resource *surface, uint32_t id );
        static void handleSetAsBackground( ::wl_client *client, struct wl_resource *resource, struct ::wl_resource *surface, struct ::wl_resource *output, uint32_t id );
        static void handleSetAsPanel( ::wl_client *client, struct wl_resource *resource, struct ::wl_resource *surface, struct ::wl_resource *output, uint32_t anchor, uint32_t id );
        static void handleSetAsDropdown( ::wl_client *client, struct wl_resource *resource, struct ::wl_resource *surface, uint32_t id );
        static void handleSetAsNotification( ::wl_client *client, struct wl_resource *resource, struct ::wl_resource *surface, uint32_t id );

        std::multimap<struct ::wl_client *, Resource *> m_resource_map;
        Resource *m_resource         = nullptr;
        struct ::wl_global *m_global = nullptr;
        uint32_t m_globalVersion;
        struct DisplayDestroyedListener : ::wl_listener {
            WayfireDesktopShell *parent;
        };
        DisplayDestroyedListener m_displayDestroyedListener;
};

class Wayland::Server::WayfireShellSurface {
    public:
        WayfireShellSurface( struct ::wl_display *display, int version );
        virtual ~WayfireShellSurface();

        class Resource {
            public:
                Resource() : WayfireShellSurfaceObject( nullptr ), handle( nullptr ) {}
                virtual ~Resource() {}

                WayfireShellSurface *WayfireShellSurfaceObject;
                WayfireShellSurface *object() { return WayfireShellSurfaceObject; }
                struct ::wl_resource *handle;

                struct ::wl_client *client() const { return wl_resource_get_client( handle ); }
                int version() const { return wl_resource_get_version( handle ); }

                static Resource *fromResource( struct ::wl_resource *resource );
        };

        void init( struct ::wl_display *display, int version );
        Resource *add( struct ::wl_client *client, int id, int version );

        Resource *resource() { return m_resource; }
        const Resource *resource() const { return m_resource; }

        std::multimap<struct ::wl_client *, Resource *> resourceMap() { return m_resource_map; }
        const std::multimap<struct ::wl_client *, Resource *> resourceMap() const { return m_resource_map; }

        bool isGlobal() const { return m_global != nullptr; }
        bool isResource() const { return m_resource != nullptr; }

        static const struct ::wl_interface *interface();

        static std::string interfaceName() { return interface()->name; }
        static int interfaceVersion() { return interface()->version; }


    protected:
        virtual Resource *allocate();

        virtual void bindResource( Resource *resource );
        virtual void destroyResource( Resource *resource );

        virtual void setLayer( Resource *resource, uint32_t layer );
        virtual void setAnchor( Resource *resource, uint32_t edges );
        virtual void setKeyboardInteraction( Resource *resource, uint32_t strength );
        virtual void setMouseInteraction( Resource *resource, uint32_t strength );
        virtual void setExclusiveZone( Resource *resource, int32_t zone );
        virtual void setMargins( Resource *resource, int32_t top, int32_t right, int32_t bottom, int32_t left );

    private:
        static void bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id );
        static void destroy_func( struct ::wl_resource *client_resource );
        static void display_destroy_func( struct ::wl_listener *listener, void *data );

        Resource *bind( struct ::wl_client *client, uint32_t id, int version );
        Resource *bind( struct ::wl_resource *handle );

        static const struct ::wayfire_shell_surface_interface m_wayfire_shell_surface_interface;

        static void handleSetLayer( ::wl_client *client, struct wl_resource *resource, uint32_t layer );
        static void handleSetAnchor( ::wl_client *client, struct wl_resource *resource, uint32_t edges );
        static void handleSetKeyboardInteraction( ::wl_client *client, struct wl_resource *resource, uint32_t strength );
        static void handleSetMouseInteraction( ::wl_client *client, struct wl_resource *resource, uint32_t strength );
        static void handleSetExclusiveZone( ::wl_client *client, struct wl_resource *resource, int32_t zone );
        static void handleSetMargins( ::wl_client *client, struct wl_resource *resource, int32_t top, int32_t right, int32_t bottom, int32_t left );

        std::multimap<struct ::wl_client *, Resource *> m_resource_map;
        Resource *m_resource         = nullptr;
        struct ::wl_global *m_global = nullptr;
        uint32_t m_globalVersion;
        struct DisplayDestroyedListener : ::wl_listener {
            WayfireShellSurface *parent;
        };
        DisplayDestroyedListener m_displayDestroyedListener;
};
