/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include "wayland-server-core.h"
#include "wayfire-desktop-shell.h"

#include <wayfire/output.hpp>
#include <wayfire/signal-definitions.hpp>

#include <map>
#include <string>

namespace Wayland {
    namespace Server {
        class WayfireOutput;
    }
}

class Wayland::Server::WayfireOutput {
    public:
        WayfireOutput( wf::output_t *output, struct ::wl_client *client, uint32_t id, int version );
        WayfireOutput( wf::output_t *output, struct ::wl_display *display, int version );
        WayfireOutput( wf::output_t *output, struct ::wl_resource *resource );
        WayfireOutput( wf::output_t *output );

        virtual ~WayfireOutput();

        class Resource {
            public:
                Resource() : wayfireOutputObject( nullptr ), handle( nullptr ) {}
                virtual ~Resource() {}

                WayfireOutput *wayfireOutputObject;
                WayfireOutput *object() { return wayfireOutputObject; }
                struct ::wl_resource *handle;

                struct ::wl_client *client() const { return wl_resource_get_client( handle ); }
                int version() const { return wl_resource_get_version( handle ); }

                static Resource *fromResource( struct ::wl_resource *resource );
        };

        void init( struct ::wl_client *client, uint32_t id, int version );
        void init( struct ::wl_display *display, int version );
        void init( struct ::wl_resource *resource );

        Resource *add( struct ::wl_client *client, int version );
        Resource *add( struct ::wl_client *client, uint32_t id, int version );
        Resource *add( struct wl_list *resource_list, struct ::wl_client *client, uint32_t id, int version );

        Resource *resource() { return m_resource; }
        const Resource *resource() const { return m_resource; }

        std::multimap<struct ::wl_client *, Resource *> resourceMap() { return m_resource_map; }
        const std::multimap<struct ::wl_client *, Resource *> resourceMap() const { return m_resource_map; }

        bool isGlobal() const { return m_global != nullptr; }
        bool isResource() const { return m_resource != nullptr; }

        static const struct ::wl_interface *interface();

        static std::string interfaceName() { return interface()->name; }
        static int interfaceVersion() { return interface()->version; }


        enum class hotspot_edge {
            hotspot_edge_top    = 1,
            hotspot_edge_bottom = 2,
            hotspot_edge_left   = 4,
            hotspot_edge_right  = 8,
        };

        void sendActiveChanged( uint32_t active );
        void sendActiveChanged( struct ::wl_resource *resource, uint32_t active );
        void sendStateChanged( uint32_t state );
        void sendStateChanged( struct ::wl_resource *resource, uint32_t state );
        void sendWsetChanged( uint32_t wset_id );
        void sendWsetChanged( struct ::wl_resource *resource, uint32_t wset_id );
        void sendWsetExtentsChanged( uint32_t wset_id, int32_t rows, int32_t columns );
        void sendWsetExtentsChanged( struct ::wl_resource *resource, uint32_t wset_id, int32_t rows, int32_t columns );
        void sendWorkspaceChanged( int32_t row, int32_t column );
        void sendWorkspaceChanged( struct ::wl_resource *resource, int32_t row, int32_t column );
        void sendDestroyed();
        void sendDestroyed( struct ::wl_resource *resource );

    protected:
        virtual Resource *allocate();

        virtual void bindResource( Resource *resource );
        virtual void destroyResource( Resource *resource );

        virtual void getState( Resource *resource );
        virtual void setState( Resource *resource );
        virtual void activate( Resource *resource, int32_t center_mouse );
        virtual void createHotspot( Resource *resource, uint32_t hotspot, uint32_t threshold, uint32_t timeout, uint32_t id );
        virtual void getActiveWset( Resource *resource );
        virtual void getWsetExtents( Resource *resource );
        virtual void getActiveWorkspace( Resource *resource );
        virtual void setActiveWorkspace( Resource *resource, int32_t row, int32_t column );

    private:
        static void bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id );
        static void destroy_func( struct ::wl_resource *client_resource );
        static void display_destroy_func( struct ::wl_listener *listener, void *data );

        Resource *bind( struct ::wl_client *client, uint32_t id, int version );
        Resource *bind( struct ::wl_resource *handle );

        static const struct ::wayfire_output_interface m_wayfire_output_interface;

        static void handleGetState( ::wl_client *, struct wl_resource *resource );
        static void handleSetState( ::wl_client *, struct wl_resource *resource );
        static void handleActivate( ::wl_client *, struct wl_resource *resource, int32_t centerMouse );
        static void handleCreateHotspot( ::wl_client *, struct wl_resource *resource, uint32_t hotspot, uint32_t threshold, uint32_t timeout, uint32_t id );
        static void handleGetActiveWset( ::wl_client *, struct wl_resource *resource );
        static void handleGetWsetExtents( ::wl_client *, struct wl_resource *resource );
        static void handleGetActiveWorkspace( ::wl_client *, struct wl_resource *resource );
        static void handleSetActiveWorkspace( ::wl_client *, struct wl_resource *resource, int32_t row, int32_t column );

        std::multimap<struct ::wl_client *, Resource *> m_resource_map;
        Resource *m_resource;
        struct ::wl_global *m_global;
        struct DisplayDestroyedListener : ::wl_listener {
            WayfireOutput *parent;
        };
        DisplayDestroyedListener m_displayDestroyedListener;

        /** Wayfire's private variables and functions */

        /** Create various connections */
        void initConnections();

        /** Disconnect from the underlying output */
        void disconnectFromOutput();

        uint32_t num_inhibits = 0;
        wf::output_t *mWfOutput;

        /** This output was destroyed */
        wf::signal::connection_t<wf::output_removed_signal> on_output_removed;

        /** To know when a view enters/exits fullscreen on this output */
        wf::signal::connection_t<wf::fullscreen_layer_focused_signal> on_fullscreen_layer_focused;

        /** Workspace extents changed */
        wf::signal::connection_t<wf::workspace_grid_changed_signal> on_workspace_grid_changed;

        /** Active workspace changed */
        wf::signal::connection_t<wf::workspace_changed_signal> on_workspace_changed;

        /** Active workspace-set changed */
        wf::signal::connection_t<wf::workspace_set_changed_signal> on_wset_changed;
};

// class Wayland::Server::WayfireOutput {
//     public:
//         WayfireOutput( wf::output_t *output, struct ::wl_client *client, int id, int version );
//         virtual ~WayfireOutput();
//
//         class Resource {
//             public:
//                 Resource() : WayfireOutputObject( nullptr ), handle( nullptr ) {}
//                 virtual ~Resource() {}
//
//                 WayfireOutput *WayfireOutputObject;
//                 WayfireOutput *object() { return WayfireOutputObject; }
//                 struct ::wl_resource *handle;
//
//                 struct ::wl_client *client() const { return wl_resource_get_client( handle ); }
//                 int version() const { return wl_resource_get_version( handle ); }
//
//                 static Resource *fromResource( struct ::wl_resource *resource );
//         };
//
//         void init( struct ::wl_client *client, int id, int version );
//         Resource *add( struct ::wl_client *client, int id, int version );
//
//         Resource *resource() { return m_resource; }
//         const Resource *resource() const { return m_resource; }
//
//         std::multimap<struct ::wl_client *, Resource *> resourceMap() { return m_resource_map; }
//         const std::multimap<struct ::wl_client *, Resource *> resourceMap() const { return
// m_resource_map; }
//
//         bool isGlobal() const { return m_global != nullptr; }
//         bool isResource() const { return m_resource != nullptr; }
//
//         static const struct ::wl_interface *interface();
//
//         static std::string interfaceName() { return interface()->name; }
//         static int interfaceVersion() { return interface()->version; }
//
//
//         void sendActiveChanged( uint32_t active );
//         void sendActiveChanged( struct ::wl_resource *resource, uint32_t active );
//         void sendStateChanged( uint32_t state );
//         void sendStateChanged( struct ::wl_resource *resource, uint32_t state );
//         void sendWsetChanged( uint32_t wset_id );
//         void sendWsetChanged( struct ::wl_resource *resource, uint32_t wset_id );
//         void sendWsetExtentsChanged( uint32_t wset_id, int32_t rows, int32_t columns );
//         void sendWsetExtentsChanged( struct ::wl_resource *resource, uint32_t wset_id, int32_t rows,
// int32_t columns );
//         void sendActiveWorkspaceChanged( int32_t row, int32_t column );
//         void sendActiveWorkspaceChanged( struct ::wl_resource *resource, int32_t row, int32_t column );
//         void sendViewAdded( uint32_t view_uuid, uint32_t wset, int32_t row, int32_t column );
//         void sendViewAdded( struct ::wl_resource *resource, uint32_t view_uuid, uint32_t wset, int32_t
// row, int32_t column );
//         void sendViewRemoved( uint32_t view_uuid, uint32_t wset, int32_t row, int32_t column );
//         void sendViewRemoved( struct ::wl_resource *resource, uint32_t view_uuid, uint32_t wset, int32_t
// row, int32_t column );
//         void sendViewChanged( uint32_t view_uuid, int32_t row, int32_t column );
//         void sendViewChanged( struct ::wl_resource *resource, uint32_t view_uuid, int32_t row, int32_t
// column );
//         void sendDestroyed();
//         void sendDestroyed( struct ::wl_resource *resource );
//
//     protected:
//         virtual Resource *allocate();
//
//         virtual void bindResource( Resource *resource );
//         virtual void destroyResource( Resource *resource );
//
//         virtual void getState( Resource *resource );
//         virtual void setState( Resource *resource );
//         virtual void activate( Resource *resource );
//         virtual void createHotspot( Resource *resource, uint32_t hotspot, uint32_t threshold, uint32_t
// timeout, uint32_t id );
//         virtual void getActiveWset( Resource *resource );
//         virtual void getWsetExtents( Resource *resource );
//         virtual void getActiveWorkspace( Resource *resource );
//         virtual void setActiveWorkspace( Resource *resource, int32_t row, int32_t column );
//
//     private:
//         static void bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id );
//         static void destroy_func( struct ::wl_resource *client_resource );
//         static void display_destroy_func( struct ::wl_listener *listener, void *data );
//
//         Resource *bind( struct ::wl_client *client, uint32_t id, int version );
//         Resource *bind( struct ::wl_resource *handle );
//
//         static const struct ::wayfire_output_interface m_wayfire_output_interface;
//
//         static void handleGetState( ::wl_client *client, struct wl_resource *resource );
//         static void handleSetState( ::wl_client *client, struct wl_resource *resource );
//         static void handleActivate( ::wl_client *client, struct wl_resource *resource );
//         static void handleCreateHotspot( ::wl_client *client, struct wl_resource *resource, uint32_t
// hotspot, uint32_t threshold, uint32_t timeout, uint32_t id );
//         static void handleGetActiveWset( ::wl_client *client, struct wl_resource *resource );
//         static void handleGetWsetExtents( ::wl_client *client, struct wl_resource *resource );
//         static void handleGetActiveWorkspace( ::wl_client *client, struct wl_resource *resource );
//         static void handleSetActiveWorkspace( ::wl_client *client, struct wl_resource *resource, int32_t
// row, int32_t column );
//
//         std::multimap<struct ::wl_client *, Resource *> m_resource_map;
//         Resource *m_resource         = nullptr;
//         struct ::wl_global *m_global = nullptr;
//         uint32_t m_globalVersion;
//         struct DisplayDestroyedListener : ::wl_listener {
//             WayfireOutput *parent;
//         };
//         DisplayDestroyedListener m_displayDestroyedListener;
//
//         /** Wayfire's private variables and functions */
//
//         /** Create various connections */
//         void initConnections();
//
//         /** Disconnect from the underlying output */
//         void disconnectFromOutput();
//
//         uint32_t num_inhibits = 0;
//         wf::output_t *output;
//
//         /** This output was destroyed */
//         wf::signal::connection_t<wf::output_removed_signal> on_output_removed;
//
//         /** To know when a view enters/exits fullscreen on this output */
//         wf::signal::connection_t<wf::fullscreen_layer_focused_signal> on_fullscreen_layer_focused;
//
//         /** Workspace extents changed */
//         wf::signal::connection_t<wf::workspace_grid_changed_signal> on_workspace_grid_changed;
//
//         /** Active workspace changed */
//         wf::signal::connection_t<wf::workspace_changed_signal> on_workspace_changed;
//
//         /** A view was moved from one workspace to another */
//         wf::signal::connection_t<wf::view_change_workspace_signal> on_view_workspace_changed;
//
//         /** A view was added: check it's output and emit the signal */
//         wf::signal::connection_t<wf::view_set_output_signal> on_view_set_output;
//
//         /** A view moved from one output to another */
//         wf::signal::connection_t<wf::view_moved_to_wset_signal> on_view_moved_to_wset;
//
//         /** A view was set/unset as sticky. */
//         wf::signal::connection_t<wf::view_set_sticky_signal> on_view_sticky_changed;
// };
