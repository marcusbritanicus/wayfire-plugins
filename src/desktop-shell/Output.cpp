/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayland-client.h>
#include <wayfire/output.hpp>
#include <wayfire/core.hpp>
#include <wayfire/seat.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/render-manager.hpp>
#include <wayfire/workspace-set.hpp>

#include "Output.hpp"
#include "Hotspot.hpp"


Wayland::Server::WayfireOutput::WayfireOutput( wf::output_t *output, struct ::wl_client *client, uint32_t id, int version ) {
    mWfOutput = output;

    m_resource_map.clear();
    init( client, id, version );

    initConnections();

    /** Conect the signal callbacks: Core */
    wf::get_core().output_layout->connect( &on_output_removed );

    /** Conect the signal callbacks: Current output */
    // output->connect( &on_fullscreen_layer_focused );
    output->connect( &on_workspace_grid_changed );
    output->connect( &on_workspace_changed );
    output->connect( &on_wset_changed );
}


Wayland::Server::WayfireOutput::WayfireOutput( wf::output_t *output, struct ::wl_display *display, int version ) {
    mWfOutput = output;

    m_resource_map.clear();
    init( display, version );

    initConnections();

    /** Conect the signal callbacks: Core */
    wf::get_core().output_layout->connect( &on_output_removed );

    /** Conect the signal callbacks: Current output */
    // output->connect( &on_fullscreen_layer_focused );
    output->connect( &on_workspace_grid_changed );
    output->connect( &on_workspace_changed );
    output->connect( &on_wset_changed );
}


Wayland::Server::WayfireOutput::WayfireOutput( wf::output_t *output, struct ::wl_resource *resource ) {
    mWfOutput = output;

    m_resource_map.clear();
    init( resource );

    initConnections();

    /** Conect the signal callbacks: Core */
    wf::get_core().output_layout->connect( &on_output_removed );

    /** Conect the signal callbacks: Current output */
    // output->connect( &on_fullscreen_layer_focused );
    output->connect( &on_workspace_grid_changed );
    output->connect( &on_workspace_changed );
    output->connect( &on_wset_changed );
}


Wayland::Server::WayfireOutput::WayfireOutput( wf::output_t *output ) {
    mWfOutput = output;

    m_resource_map.clear();

    initConnections();

    /** Conect the signal callbacks: Core */
    wf::get_core().output_layout->connect( &on_output_removed );

    /** Conect the signal callbacks: Current output */
    // output->connect( &on_fullscreen_layer_focused );
    output->connect( &on_workspace_grid_changed );
    output->connect( &on_workspace_changed );
    output->connect( &on_wset_changed );
}


Wayland::Server::WayfireOutput::~WayfireOutput() {
    for (auto it = m_resource_map.begin(); it != m_resource_map.end(); ) {
        Resource *resourcePtr = it->second;

        // Delete the Resource object pointed to by resourcePtr
        resourcePtr->wayfireOutputObject = nullptr;
    }

    if ( m_resource ) {
        m_resource->wayfireOutputObject = nullptr;
    }

    if ( m_global ) {
        wl_global_destroy( m_global );
        wl_list_remove( &m_displayDestroyedListener.link );
    }

    delete mWfOutput;
}


void Wayland::Server::WayfireOutput::init( struct ::wl_client *client, uint32_t id, int version ) {
    m_resource = bind( client, id, version );
}


void Wayland::Server::WayfireOutput::init( struct ::wl_resource *resource ) {
    m_resource = bind( resource );
}


Wayland::Server::WayfireOutput::Resource *Wayland::Server::WayfireOutput::add( struct ::wl_client *client, int version ) {
    Resource *resource = bind( client, 0, version );

    m_resource_map.insert( std::pair { client, resource } );
    return resource;
}


Wayland::Server::WayfireOutput::Resource *Wayland::Server::WayfireOutput::add( struct ::wl_client *client, uint32_t id, int version ) {
    Resource *resource = bind( client, id, version );

    m_resource_map.insert( std::pair { client, resource } );
    return resource;
}


void Wayland::Server::WayfireOutput::init( struct ::wl_display *display, int version ) {
    m_global = wl_global_create( display, &::wayfire_output_interface, version, this, bind_func );
    m_displayDestroyedListener.notify = WayfireOutput::display_destroy_func;
    m_displayDestroyedListener.parent = this;
    wl_display_add_destroy_listener( display, &m_displayDestroyedListener );
}


const struct wl_interface *Wayland::Server::WayfireOutput::interface() {
    return &::wayfire_output_interface;
}


Wayland::Server::WayfireOutput::Resource *Wayland::Server::WayfireOutput::allocate() {
    return new Resource;
}


void Wayland::Server::WayfireOutput::bindResource( Resource * ) {
}


void Wayland::Server::WayfireOutput::destroyResource( Resource * ) {
}


void Wayland::Server::WayfireOutput::bind_func( struct ::wl_client *client, void *data, uint32_t version, uint32_t id ) {
    WayfireOutput *that = static_cast<WayfireOutput *>(data);

    that->add( client, id, version );
}


void Wayland::Server::WayfireOutput::display_destroy_func( struct ::wl_listener *listener, void * ) {
    WayfireOutput *that = static_cast<WayfireOutput::DisplayDestroyedListener *>(listener)->parent;

    that->m_global = nullptr;
}


void Wayland::Server::WayfireOutput::destroy_func( struct ::wl_resource *client_resource ) {
    Resource      *resource = Resource::fromResource( client_resource );
    WayfireOutput *that     = resource->wayfireOutputObject;

    if ( that ) {
        auto it = that->m_resource_map.begin();
        while ( it != that->m_resource_map.end() ) {
            if ( it->first == resource->client() ) {
                it = that->m_resource_map.erase( it );
            }

            else {
                ++it;
            }
        }
        that->destroyResource( resource );

        that = resource->wayfireOutputObject;

        if ( that && (that->m_resource == resource) ) {
            that->m_resource = nullptr;
        }
    }

    delete resource;
}


Wayland::Server::WayfireOutput::Resource *Wayland::Server::WayfireOutput::bind( struct ::wl_client *client, uint32_t id, int version ) {
    struct ::wl_resource *handle = wl_resource_create( client, &::wayfire_output_interface, version, id );

    return bind( handle );
}


Wayland::Server::WayfireOutput::Resource *Wayland::Server::WayfireOutput::bind( struct ::wl_resource *handle ) {
    Resource *resource = allocate();

    resource->wayfireOutputObject = this;

    wl_resource_set_implementation( handle, &m_wayfire_output_interface, resource, destroy_func );
    resource->handle = handle;
    bindResource( resource );
    return resource;
}


Wayland::Server::WayfireOutput::Resource *Wayland::Server::WayfireOutput::Resource::fromResource( struct ::wl_resource *resource ) {
    if ( !resource ) {
        return nullptr;
    }

    if ( wl_resource_instance_of( resource, &::wayfire_output_interface, &m_wayfire_output_interface ) ) {
        return static_cast<Resource *>(wl_resource_get_user_data( resource ) );
    }

    return nullptr;
}


const struct ::wayfire_output_interface Wayland::Server::WayfireOutput::m_wayfire_output_interface = {
    Wayland::Server::WayfireOutput::handleGetState,
    Wayland::Server::WayfireOutput::handleSetState,
    Wayland::Server::WayfireOutput::handleActivate,
    Wayland::Server::WayfireOutput::handleCreateHotspot,
    Wayland::Server::WayfireOutput::handleGetActiveWset,
    Wayland::Server::WayfireOutput::handleGetWsetExtents,
    Wayland::Server::WayfireOutput::handleGetActiveWorkspace,
    Wayland::Server::WayfireOutput::handleSetActiveWorkspace
};

void Wayland::Server::WayfireOutput::getState( Resource * ) {
}


void Wayland::Server::WayfireOutput::setState( Resource * ) {
}


void Wayland::Server::WayfireOutput::activate( Resource *, int32_t centerMouse ) {
    wf::get_core().seat->focus_output( mWfOutput );
    mWfOutput->ensure_pointer( centerMouse );
}


void Wayland::Server::WayfireOutput::createHotspot( Resource *resource, uint32_t hotspot, uint32_t threshold, uint32_t timeout, uint32_t id ) {
    new Wayland::Server::WayfireHotspot( mWfOutput, hotspot, threshold, timeout, wl_resource_get_client( resource->handle ), id, 1 );
}


void Wayland::Server::WayfireOutput::getActiveWset( Resource * ) {
    sendWsetChanged( mWfOutput->wset()->get_index() );
}


void Wayland::Server::WayfireOutput::getWsetExtents( Resource * ) {
    wf::dimensions_t dims = mWfOutput->wset()->get_workspace_grid_size();

    sendWsetExtentsChanged( mWfOutput->wset()->get_index(), dims.height, dims.width );
}


void Wayland::Server::WayfireOutput::getActiveWorkspace( Resource * ) {
    wf::point_t curWS = mWfOutput->wset()->get_current_workspace();

    sendWorkspaceChanged( curWS.y, curWS.x );
}


void Wayland::Server::WayfireOutput::setActiveWorkspace( Resource *, int32_t row, int32_t col ) {
    /** This will trigger the workspace_changed_signal, which will emit the signal for us */
    mWfOutput->wset()->request_workspace( wf::point_t { col, row } );
}


void Wayland::Server::WayfireOutput::handleGetState( ::wl_client *, struct wl_resource *resource ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireOutputObject ) {
        return;
    }

    static_cast<WayfireOutput *>(r->wayfireOutputObject)->getState( r );
}


void Wayland::Server::WayfireOutput::handleSetState( ::wl_client *, struct wl_resource *resource ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireOutputObject ) {
        return;
    }

    static_cast<WayfireOutput *>(r->wayfireOutputObject)->setState( r );
}


void Wayland::Server::WayfireOutput::handleActivate( ::wl_client *, struct wl_resource *resource, int32_t centerMouse ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireOutputObject ) {
        return;
    }

    static_cast<WayfireOutput *>(r->wayfireOutputObject)->activate( r, centerMouse );
}


void Wayland::Server::WayfireOutput::handleCreateHotspot( ::wl_client *, struct wl_resource *resource, uint32_t hotspot, uint32_t threshold, uint32_t timeout, uint32_t id ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireOutputObject ) {
        return;
    }

    static_cast<WayfireOutput *>(r->wayfireOutputObject)->createHotspot( r, hotspot, threshold, timeout, id );
}


void Wayland::Server::WayfireOutput::handleGetActiveWset( ::wl_client *, struct wl_resource *resource ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireOutputObject ) {
        return;
    }

    static_cast<WayfireOutput *>(r->wayfireOutputObject)->getActiveWset( r );
}


void Wayland::Server::WayfireOutput::handleGetWsetExtents( ::wl_client *, struct wl_resource *resource ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireOutputObject ) {
        return;
    }

    static_cast<WayfireOutput *>(r->wayfireOutputObject)->getWsetExtents( r );
}


void Wayland::Server::WayfireOutput::handleGetActiveWorkspace( ::wl_client *, struct wl_resource *resource ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireOutputObject ) {
        return;
    }

    static_cast<WayfireOutput *>(r->wayfireOutputObject)->getActiveWorkspace( r );
}


void Wayland::Server::WayfireOutput::handleSetActiveWorkspace( ::wl_client *, struct wl_resource *resource, int32_t row, int32_t column ) {
    Resource *r = Resource::fromResource( resource );

    if ( !r->wayfireOutputObject ) {
        return;
    }

    static_cast<WayfireOutput *>(r->wayfireOutputObject)->setActiveWorkspace( r, row, column );
}


void Wayland::Server::WayfireOutput::sendActiveChanged( uint32_t active ) {
    if ( !m_resource ) {
        return;
    }

    sendActiveChanged( m_resource->handle, active );
}


void Wayland::Server::WayfireOutput::sendActiveChanged( struct ::wl_resource *resource, uint32_t active ) {
    wayfire_output_send_active_changed( resource, active );
}


void Wayland::Server::WayfireOutput::sendStateChanged( uint32_t state ) {
    if ( !m_resource ) {
        return;
    }

    sendStateChanged( m_resource->handle, state );
}


void Wayland::Server::WayfireOutput::sendStateChanged( struct ::wl_resource *resource, uint32_t state ) {
    wayfire_output_send_state_changed( resource, state );
}


void Wayland::Server::WayfireOutput::sendWsetChanged( uint32_t wset_id ) {
    if ( !m_resource ) {
        return;
    }

    sendWsetChanged( m_resource->handle, wset_id );
}


void Wayland::Server::WayfireOutput::sendWsetChanged( struct ::wl_resource *resource, uint32_t wset_id ) {
    wayfire_output_send_wset_changed( resource, wset_id );
}


void Wayland::Server::WayfireOutput::sendWsetExtentsChanged( uint32_t wset_id, int32_t rows, int32_t columns ) {
    if ( !m_resource ) {
        return;
    }

    sendWsetExtentsChanged( m_resource->handle, wset_id, rows, columns );
}


void Wayland::Server::WayfireOutput::sendWsetExtentsChanged( struct ::wl_resource *resource, uint32_t wset_id, int32_t rows, int32_t columns ) {
    wayfire_output_send_wset_extents_changed( resource, wset_id, rows, columns );
}


void Wayland::Server::WayfireOutput::sendWorkspaceChanged( int32_t row, int32_t column ) {
    if ( !m_resource ) {
        return;
    }

    sendWorkspaceChanged( m_resource->handle, row, column );
}


void Wayland::Server::WayfireOutput::sendWorkspaceChanged( struct ::wl_resource *resource, int32_t row, int32_t column ) {
    wayfire_output_send_workspace_changed( resource, row, column );
}


void Wayland::Server::WayfireOutput::sendDestroyed() {
    if ( !m_resource ) {
        return;
    }

    sendDestroyed( m_resource->handle );
}


void Wayland::Server::WayfireOutput::sendDestroyed( struct ::wl_resource *resource ) {
    wayfire_output_send_destroyed( resource );
}


// void Wayland::Server::WayfireOutput::listAllOutputViews() {
//     wf::dimensions_t grid = output->wset()->get_workspace_grid_size();
//
//     for ( wayfire_toplevel_view view: output->wset()->get_views( wf::WSET_MAPPED_ONLY |
// wf::WSET_SORT_STACKING ) ) {
//         /** We don't care for non-toplevel views */
//         if ( view->role != wf::VIEW_ROLE_TOPLEVEL ) {
//             continue;
//         }
//
//         /** If the view is sticky, send view_added signal for all workspaces */
//         if ( view->sticky ) {
//             for ( int r = 0; r < grid.height; r++ ) {
//                 for ( int c = 0; c < grid.width; c++ ) {
//                     zdesq_output_v1_send_view_added( resource, view->get_id(), r, c );
//                 }
//             }
//         }
//
//         /** The view is mainly on one workspace */
//         else {
//             wf::point_t ws = output->wset()->get_view_main_workspace( view );
//             zdesq_output_v1_send_view_added( resource, view->get_id(), ws.y, ws.x );
//         }
//     }
// }


void Wayland::Server::WayfireOutput::disconnectFromOutput() {
    wf::get_core().output_layout->disconnect( &on_output_removed );
    on_fullscreen_layer_focused.disconnect();
}


void Wayland::Server::WayfireOutput::initConnections() {
    on_output_removed =
        [ = ] (wf::output_removed_signal *ev){
            if ( ev->output == mWfOutput ) {
                sendDestroyed();

                disconnectFromOutput();
                mWfOutput = nullptr;
            }
        };

    // on_fullscreen_layer_focused =
    //     [ = ] (wf::fullscreen_layer_focused_signal *ev){
    //         if ( ev->has_promoted ) {
    //             // zdesq_output_v1_send_enter_fullscreen( resource );
    //             send
    //         }
    //
    //         else {
    //             // zdesq_output_v1_send_leave_fullscreen( resource );
    //         }
    //     };

    on_workspace_grid_changed =
        [ = ] (wf::workspace_grid_changed_signal *ev) {
            wf::point_t curWS = mWfOutput->wset()->get_current_workspace();

            /** Send the workspace_extents signal */
            sendWsetExtentsChanged( mWfOutput->wset()->get_index(), ev->new_grid_size.height, ev->new_grid_size.width );

            /** Send the active_workspace signal, because the active workspace might have changed */
            sendWorkspaceChanged( curWS.y, curWS.x );
        };

    on_workspace_changed =
        [ = ] (wf::workspace_changed_signal *ev) {
            /** Send the active_workspace signal */
            sendWorkspaceChanged( ev->new_viewport.y, ev->new_viewport.x );
        };

    on_wset_changed =
        [ = ] (wf::workspace_set_changed_signal *ev) {
            /** Send the active_workspace signal */
            sendWsetChanged( ev->new_wset->get_index() );

            /** Send the workspace_extents signal */
            wf::dimensions_t new_grid_size = ev->new_wset->get_workspace_grid_size();
            sendWsetExtentsChanged( ev->new_wset->get_index(), new_grid_size.height, new_grid_size.width );

            /** Send the active_workspace signal, because the active workspace might have changed */
            wf::point_t curWS = ev->new_wset->get_current_workspace();
            sendWorkspaceChanged( curWS.y, curWS.x );
        };
}
